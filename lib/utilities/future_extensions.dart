import 'dart:async';

extension OnError<T> on Future<T> {
  Future<T> onErrorJustReturn(T value) => catchError((_) => value);
}

extension OnErrorNullable<T> on Future<T?> {
  Future<T?> drainErrorsNullable() => catchError((_) => null);

  Future<T?> drainErrorsAll() {
    final completer = Completer<T?>();

    then(
      (value) => completer.complete(value),
      onError: (_) => completer.complete(null),
    );

    return completer.future;
  }
}

Future<T> retry<T>(
    {required Future<T> Function() future,
    int maxCount = 5,
    Duration delay = Duration.zero}) async {
  try {
    return await future();
  } catch (_) {
    if (maxCount > 1) {
      return Future.delayed(delay).then(
          (_) => retry(future: future, delay: delay, maxCount: maxCount - 1));
    } else {
      rethrow;
    }
  }
}
