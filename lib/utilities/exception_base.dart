import 'package:flutter/foundation.dart' show immutable;

@immutable
sealed class ExceptionBase {
  final String? dialogTitle;
  final String? dialogText;

  const ExceptionBase({
    this.dialogTitle,
    this.dialogText,
  });
}

abstract class DataExceptionBase extends ExceptionBase {
  const DataExceptionBase({super.dialogTitle, super.dialogText});
}

abstract class StorageExceptionBase extends ExceptionBase {
  const StorageExceptionBase({super.dialogTitle, super.dialogText});
}
