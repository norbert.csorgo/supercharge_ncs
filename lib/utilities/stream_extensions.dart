import 'package:rxdart/transformers.dart';
import 'package:supercharge_ncs/res/enums.dart';
import 'package:supercharge_ncs/res/types.dart';

extension UnwrapStream<T> on Stream<T?> {
  Stream<T> unwrap() => where((event) => event != null).cast();
}

extension Loading<E> on Stream<E> {
  Stream<E> startLoading({
    required Sink<LoadingInfo> onSink,
    LoadingEvent? comment,
  }) =>
      doOnEach(
        (_) => onSink.add((isLoading: true, comment: comment)),
      );
  Stream<E> startNullAwareLoading({
    required Sink<LoadingInfo> onSink,
    LoadingEvent? comment,
  }) =>
      doOnEach(
        (event) => (event.isOnData && event.requireData == null)
            ? null
            : onSink.add((isLoading: true, comment: comment)),
      );
  Stream<E> stopLoading({
    required Sink<LoadingInfo> onSink,
  }) =>
      debounceTime(const Duration(milliseconds: 500)).doOnEach(
        (_) => onSink.add((isLoading: false, comment: null)),
      );
  Stream<E> stopNullAwareLoading({
    required Sink<LoadingInfo> onSink,
  }) =>
      debounceTime(const Duration(milliseconds: 500)).doOnEach(
        (event) => (event.isOnData && event.requireData == null)
            ? null
            : onSink.add((isLoading: false, comment: null)),
      );
}
