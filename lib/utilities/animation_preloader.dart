import 'package:flutter/material.dart';
import 'package:rive/rive.dart';
import 'package:supercharge_ncs/res/app_consts.dart';
import 'package:supercharge_ncs/res/gen/assets.gen.dart';
import 'package:supercharge_ncs/utilities/future_extensions.dart';

class AnimationPreloader extends ChangeNotifier {
  RiveFile? animationFile;

  Future<void> init() async {
    await retry<void>(
      delay: const Duration(seconds: AppConsts.time1000),
      future: () async => animationFile = await RiveFile.asset(
        Assets.animation.path,
      ),
    ).then<void>((_) => notifyListeners()).drainErrorsNullable();
  }

  @override
  void dispose() {
    animationFile = null;
    super.dispose();
  }
}
