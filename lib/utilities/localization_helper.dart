import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

abstract final class Localization {
  Localization._();
  static BuildContext? _ctx;

  static void init(BuildContext context) => _ctx = context;

  static AppLocalizations get _loc => AppLocalizations.of(_ctx!);
}

AppLocalizations get localized => Localization._loc;
