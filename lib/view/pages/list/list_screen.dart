import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:supercharge_ncs/logic/app_bloc.dart';
import 'package:supercharge_ncs/res/app_consts.dart';
import 'package:supercharge_ncs/res/app_theme.dart';
import 'package:supercharge_ncs/res/enums.dart';
import 'package:supercharge_ncs/utilities/context_extensions.dart';
import 'package:supercharge_ncs/view/components/basic_loading_indicator.dart';
import 'package:supercharge_ncs/view/pages/list/components/detailed_screen.dart';

class ListScreen extends StatelessWidget {
  const ListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final appBloc = context.read<AppBloc>();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Your personal list",
          style: context.theme.textTheme.titleLarge,
        ),
        actions: [
          IconButton(
            onPressed: () {
              appBloc.userSession.goToView.add(Views.decision);
            },
            icon: Icon(
              MdiIcons.newBox,
              color: AppTheme.gold1000,
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(
          top: AppConsts.high24,
          bottom: AppConsts.high48,
          left: AppConsts.wide24,
          right: AppConsts.wide24,
        ),
        child: Align(
          alignment: Alignment.topCenter,
          child: Card(
            elevation: AppConsts.normal6,
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: AppConsts.high16,
                horizontal: AppConsts.wide24,
              ),
              child: StreamBuilder(
                  stream: appBloc.userSession.savedBeers,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting ||
                        snapshot.connectionState == ConnectionState.none ||
                        !snapshot.hasData) {
                      return const BasicLoadingIndicator();
                    } else {
                      final beers = snapshot.requireData.toList();
                      return ListView.separated(
                        padding: EdgeInsets.zero,
                        shrinkWrap: true,
                        itemCount: beers.length,
                        separatorBuilder: (_, __) =>
                            SizedBox(height: AppConsts.high12),
                        itemBuilder: (context, index) {
                          final currentBeer = beers[index];

                          return ListTile(
                            contentPadding: EdgeInsets.zero,
                            onTap: () {
                              context.navi.push(
                                MaterialPageRoute(
                                  builder: (context) =>
                                      DetailedScreen(beer: currentBeer),
                                ),
                              );
                            },
                            leading: SizedBox.square(
                              dimension: AppConsts.rad48,
                              child: Image.network(currentBeer.imageUrl),
                            ),
                            title: Text(currentBeer.name),
                          );
                        },
                      );
                    }
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
