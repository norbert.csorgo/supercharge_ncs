import 'package:flutter/material.dart';
import 'package:supercharge_ncs/models/beer_model.dart';
import 'package:supercharge_ncs/res/app_consts.dart';
import 'package:supercharge_ncs/res/app_theme.dart';
import 'package:supercharge_ncs/utilities/context_extensions.dart';

class DetailedScreen extends StatelessWidget {
  const DetailedScreen({super.key, required this.beer});

  final BeerModel beer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: context.navi.pop,
            icon: const Icon(
              Icons.arrow_back,
              color: AppTheme.gold1000,
            ),
          ),
          title: Text(
            beer.name,
            style: context.theme.textTheme.titleLarge,
          ),
        ),
        body: Padding(
          padding: EdgeInsets.only(
            top: AppConsts.high24,
            bottom: AppConsts.high48,
            left: AppConsts.wide24,
            right: AppConsts.wide24,
          ),
          child: Align(
            alignment: Alignment.topCenter,
            child: Card(
              elevation: AppConsts.normal6,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  vertical: AppConsts.high16,
                  horizontal: AppConsts.wide24,
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      LimitedBox(
                        maxHeight: AppConsts.high240,
                        child: Image.network(beer.imageUrl),
                      ),
                      SizedBox(height: AppConsts.high24),
                      Text(beer.description),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
