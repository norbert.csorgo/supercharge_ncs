import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:supercharge_ncs/logic/app_bloc.dart';
import 'package:supercharge_ncs/res/app_consts.dart';
import 'package:supercharge_ncs/res/enums.dart';
import 'package:supercharge_ncs/res/gen/assets.gen.dart';
import 'package:supercharge_ncs/utilities/context_extensions.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(
          vertical: AppConsts.high48,
          horizontal: AppConsts.wide24,
        ),
        child: Center(
          child: Card(
            elevation: AppConsts.normal6,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: AppConsts.high16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: AppConsts.high240,
                    child: Assets.beerSlide.rive(),
                  ),
                  SizedBox(height: AppConsts.high24),
                  IntrinsicWidth(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          width: double.infinity,
                          child: ElevatedButton.icon(
                            style: context.theme.elevatedButtonTheme.style
                                ?.copyWith(alignment: Alignment.centerLeft),
                            onPressed: () {
                              final appBloc = context.read<AppBloc>();
                              appBloc.userSession.goToView.add(Views.decision);
                            },
                            icon: Icon(MdiIcons.selectSearch),
                            label: const Text("Search for new beers!"),
                          ),
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: ElevatedButton.icon(
                            style: context.theme.elevatedButtonTheme.style
                                ?.copyWith(alignment: Alignment.centerLeft),
                            onPressed: () {
                              final appBloc = context.read<AppBloc>();
                              appBloc.userSession.fetchSavedBeersSink.add(null);
                              appBloc.userSession.goToView.add(Views.list);
                            },
                            icon: Icon(MdiIcons.beer),
                            label: const Text("View last session's beers"),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
