import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:supercharge_ncs/logic/app_bloc.dart';
import 'package:supercharge_ncs/res/app_consts.dart';
import 'package:supercharge_ncs/res/enums.dart';
import 'package:supercharge_ncs/utilities/context_extensions.dart';
import 'package:supercharge_ncs/view/components/basic_loading_indicator.dart';

class DecisionScreen extends StatelessWidget {
  const DecisionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final appBloc = context.read<AppBloc>();

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(
          vertical: AppConsts.high48,
          horizontal: AppConsts.wide24,
        ),
        child: Center(
          child: Card(
            elevation: AppConsts.normal6,
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: AppConsts.high16,
                horizontal: AppConsts.wide24,
              ),
              child: StreamBuilder(
                  stream: appBloc.userSession.beers,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting ||
                        snapshot.connectionState == ConnectionState.none ||
                        !snapshot.hasData) {
                      return const BasicLoadingIndicator();
                    } else {
                      final beers = snapshot.requireData.toList();
                      return HookBuilder(
                        builder: (context) {
                          final currentIndex = useState(0);
                          final currentBeer = beers[currentIndex.value];
                          return Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: AppConsts.high240,
                                width: double.infinity,
                                child: Image.network(
                                  currentBeer.imageUrl,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                              SizedBox(height: AppConsts.high24),
                              SizedBox(
                                height: AppConsts.normal120,
                                child: Text(
                                  currentBeer.name,
                                  style: context.theme.textTheme.headlineMedium,
                                ),
                              ),
                              SizedBox(height: AppConsts.high8),
                              Text(
                                currentBeer.tagline,
                                style: context.theme.textTheme.headlineSmall,
                              ),
                              SizedBox(height: AppConsts.high48),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  ElevatedButton.icon(
                                    onPressed: () {
                                      if (currentIndex.value < 9) {
                                        currentIndex.value++;
                                      } else {
                                        appBloc.userSession.goToView
                                            .add(Views.list);
                                      }
                                    },
                                    icon: Icon(MdiIcons.cancel),
                                    label: const Text("Not for me..."),
                                  ),
                                  ElevatedButton.icon(
                                    onPressed: () {
                                      appBloc.userSession.saveBeerSink
                                          .add(currentBeer);

                                      if (currentIndex.value < 9) {
                                        currentIndex.value++;
                                      } else {
                                        appBloc.userSession.goToView
                                            .add(Views.list);
                                      }
                                    },
                                    icon: Icon(MdiIcons.check),
                                    label: const Text("This is it!"),
                                  ),
                                ],
                              ),
                            ],
                          );
                        },
                      );
                    }
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
