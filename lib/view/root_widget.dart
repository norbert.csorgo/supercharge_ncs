import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:provider/provider.dart';
import 'package:supercharge_ncs/logic/app_bloc.dart';
import 'package:supercharge_ncs/res/app_theme.dart';
import 'package:supercharge_ncs/res/enums.dart';
import 'package:supercharge_ncs/utilities/animation_preloader.dart';
import 'package:supercharge_ncs/utilities/exception_base.dart';
import 'package:supercharge_ncs/utilities/localization_helper.dart';
import 'package:supercharge_ncs/utilities/stream_extensions.dart';
import 'package:supercharge_ncs/view/components/basic_loading_indicator.dart';
import 'package:supercharge_ncs/view/components/loading_overlay.dart';
import 'package:supercharge_ncs/view/pages/decision/decision_screen.dart';
import 'package:supercharge_ncs/view/pages/list/list_screen.dart';
import 'package:supercharge_ncs/view/pages/welcome/welcome_screen.dart';

class RootWidget extends HookWidget {
  const RootWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appBloc = context.read<AppBloc>();

    useEffect(() {
      Localization.init(context);
      context.read<AnimationPreloader>().init();

      final errorSub = appBloc.appErrors.unwrap().listen(
        (error) {
          switch (error) {
            case StorageExceptionBase() || DataExceptionBase():
              break;
          }
          //showException(exception: error, context: context);
        },
      );
      final isLoadingSub = appBloc.isLoading.listen((loadingInfo) {
        if (loadingInfo.isLoading) {
          final text = switch (loadingInfo.comment) {
            LoadingEvent.fetching => "Fetching new beers...",
            LoadingEvent.saving => "Saving...",
            LoadingEvent.deleting => "Deleting...",
            null => "Almost ready...",
          };
          LoadingOverlay.show(
            context: context,
            text: text,
          );
        } else {
          LoadingOverlay.hide();
        }
      });

      return () async {
        await Future.wait([
          errorSub.cancel(),
          isLoadingSub.cancel(),
        ]);
      };
    }, []);

    return StreamBuilder<Views>(
      stream: appBloc.userSession.baseView,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none || ConnectionState.waiting:
            return const ColoredBox(
              color: AppTheme.gold200,
              child: BasicLoadingIndicator(),
            );
          case ConnectionState.active || ConnectionState.done:
            final currentView = snapshot.requireData;
            return switch (currentView) {
              Views.initial => const WelcomeScreen(),
              Views.decision => const DecisionScreen(),
              Views.list => const ListScreen(),
            };
        }
      },
    );
  }
}
