import 'dart:async';
import 'package:flutter/material.dart';

final class LoadingOverlay {
  LoadingOverlay._internal();

  static _LoadingOverlayController? _controller;

  static late final Widget _defaultLoadingWidget;
  static late final Color _defaultBackgroundColor;
  static late final Color _defaultBarrierColor;

  static late Widget _loadingWidget;
  static late Color _backgroundColor;
  static late Color _barrierColor;

  static Future<void> initialize({
    Widget defaultLoadingWidget = const CircularProgressIndicator(),
    Color defaultBackgroundColor = const Color(0xffffffff),
    Color defaultBarrierColor = const Color.fromARGB(150, 0, 0, 0),
  }) async {
    _defaultLoadingWidget = defaultLoadingWidget;
    _loadingWidget = defaultLoadingWidget;
    _defaultBackgroundColor = defaultBackgroundColor;
    _backgroundColor = defaultBackgroundColor;
    _defaultBarrierColor = defaultBarrierColor;
    _barrierColor = defaultBarrierColor;
  }

  static void setup({
    Widget? loadingWidget,
    Color? backgroundColor,
    Color? barrierColor,
  }) {
    _loadingWidget = loadingWidget ?? _loadingWidget;
    _backgroundColor = backgroundColor ?? _backgroundColor;
    _barrierColor = barrierColor ?? _barrierColor;
  }

  static void show({
    required BuildContext context,
    String? text,
  }) {
    if (_controller?.update(text) ?? false) {
      return;
    } else {
      _controller = _showOverlay(
        context: context,
        text: text,
      );
    }
  }

  static void hide() {
    _controller?.close();
    _controller = null;
    _loadingWidget = _defaultLoadingWidget;
    _backgroundColor = _defaultBackgroundColor;
    _barrierColor = _defaultBarrierColor;
  }

  static _LoadingOverlayController _showOverlay({
    required BuildContext context,
    String? text,
  }) {
    final textController = StreamController<String?>();
    textController.add(text);

    final renderBox = context.findRenderObject() as RenderBox;
    final availableSize = renderBox.size;

    final overlay = OverlayEntry(
      builder: (context) {
        return Material(
          color: _barrierColor,
          child: Center(
            child: Container(
              constraints: BoxConstraints(
                maxWidth: availableSize.width * 0.8,
                maxHeight: availableSize.height * 0.8,
              ),
              decoration: BoxDecoration(
                color: _backgroundColor,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 24.0, vertical: 16.0),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 8.0),
                      _loadingWidget,
                      const SizedBox(height: 8.0),
                      StreamBuilder<String?>(
                        stream: textController.stream,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: Text(
                                snapshot.data as String,
                                textAlign: TextAlign.center,
                              ),
                            );
                          } else {
                            return const SizedBox();
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );

    final state = Overlay.of(context);
    state.insert(overlay);

    return _LoadingOverlayController(
      close: () {
        textController.close();
        overlay.remove();
        return true;
      },
      update: (text) {
        textController.add(text);
        return true;
      },
    );
  }
}

typedef _CloseLoadingOverlay = bool Function();
typedef _UpdateLoadingOverlay = bool Function(String? string);

@immutable
class _LoadingOverlayController {
  final _CloseLoadingOverlay close;
  final _UpdateLoadingOverlay update;

  const _LoadingOverlayController({
    required this.close,
    required this.update,
  });
}
