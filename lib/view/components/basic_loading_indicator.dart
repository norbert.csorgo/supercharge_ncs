import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';
import 'package:supercharge_ncs/res/app_consts.dart';
import 'package:supercharge_ncs/res/app_theme.dart';
import 'package:supercharge_ncs/utilities/animation_preloader.dart';

class BasicLoadingIndicator extends StatelessWidget {
  const BasicLoadingIndicator({super.key, this.height, this.width});

  final double? height;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: height ?? AppConsts.rad120,
        width: width ?? AppConsts.rad120,
        child: Consumer<AnimationPreloader>(
          child: const CircularProgressIndicator(color: AppTheme.goldShadow),
          builder: (context, preloader, loadingWidget) =>
              preloader.animationFile != null
                  ? RiveAnimation.direct(
                      preloader.animationFile!,
                      artboard: RiveStrings.loadingAB,
                      fit: BoxFit.contain,
                      animations: const [RiveStrings.loadingTimeline],
                    )
                  : loadingWidget!,
        ),
      ),
    );
  }
}
