import 'package:supercharge_ncs/datasource/local/storage/storage_error.dart';
import 'package:supercharge_ncs/models/beer_model.dart';
import 'package:supercharge_ncs/objectbox.g.dart';

abstract final class LocalStorageAPIInterface {
  Future<List<BeerModel>> getSavedBeerModels();
  Future<StorageError?> saveBeerModel({
    required BeerModel beer,
  });
  Future<StorageError?> removeSavedBeerModel({
    required int id,
  });
}

final class LocalStorageAPI implements LocalStorageAPIInterface {
  const LocalStorageAPI._();
  static const LocalStorageAPI _instance = LocalStorageAPI._();
  static LocalStorageAPI get instance => _instance;

  static Store? _store;
  static Future<void>? _initialized;

  static StorageError _handleErrors(Object err) {
    if (err is StorageError) {
      return err;
    } else {
      return const StorageErrorUnknown();
    }
  }

  static Future<Store?> _ensureStoreInitialized() async {
    if (_store != null) return _store!;
    _initialized ??= Future.delayed(
      const Duration(),
      () async {
        try {
          _store = await openStore();
        } catch (err) {
          _store = null;
          throw const StorageErrorCannotOpen();
        }
      },
    );
    await _initialized;

    return _store;
  }

  @override
  Future<List<BeerModel>> getSavedBeerModels() async {
    try {
      final store = await _ensureStoreInitialized();
      final beerBox = store!.box<BeerModel>();

      return beerBox.getAll();
    } catch (_) {
      rethrow;
    }
  }

  @override
  Future<StorageError?> removeSavedBeerModel({
    required int id,
  }) async {
    try {
      final store = await _ensureStoreInitialized();
      final beerBox = store!.box<BeerModel>();

      final removed = beerBox.remove(id);

      return removed ? null : const StorageErrorCannotPerformTask();
    } catch (err) {
      return _handleErrors(err);
    }
  }

  @override
  Future<StorageError?> saveBeerModel({
    required BeerModel beer,
  }) async {
    try {
      final store = await _ensureStoreInitialized();
      final beerBox = store!.box<BeerModel>();

      beerBox.put(beer);

      return null;
    } catch (err) {
      return _handleErrors(err);
    }
  }
}
