import 'package:flutter/foundation.dart' show immutable;
import 'package:supercharge_ncs/utilities/exception_base.dart';

@immutable
sealed class StorageError extends StorageExceptionBase {
  const StorageError({
    required super.dialogTitle,
    required super.dialogText,
  });
}

@immutable
final class StorageErrorUnknown extends StorageError {
  const StorageErrorUnknown()
      : super(
          dialogTitle: 'Storage error',
          dialogText: 'Unknown storage error',
        );
}

@immutable
final class StorageErrorCannotOpen extends StorageError {
  const StorageErrorCannotOpen()
      : super(
          dialogTitle: 'Storage error',
          dialogText: 'Cannot open storage',
        );
}

@immutable
final class StorageErrorCannotPerformTask extends StorageError {
  const StorageErrorCannotPerformTask()
      : super(
          dialogTitle: 'Storage error',
          dialogText: 'Cannot perform task',
        );
}
