import 'package:supercharge_ncs/datasource/local/storage/local_storage.dart';

abstract final class LocalDatasource {
  LocalDatasource._();

  static LocalStorageAPIInterface get localStorage => LocalStorageAPI.instance;
}
