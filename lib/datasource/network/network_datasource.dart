import 'package:supercharge_ncs/datasource/network/data/data_api.dart';

abstract final class NetworkDatasource {
  NetworkDatasource._();

  static DataAPI get data => DataAPI.instance;
}
