import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:supercharge_ncs/logic/userSession/data/data_error.dart';
import 'package:supercharge_ncs/models/beer_model.dart';
import 'package:supercharge_ncs/utilities/future_extensions.dart';

final class DataAPI {
  const DataAPI._();

  static const DataAPI _instance = DataAPI._();
  static DataAPI get instance => _instance;

  Future<Iterable<BeerModel>> getBeers(int page) async {
    HttpClient httpClient = HttpClient();
    try {
      return retry<Iterable<BeerModel>>(
          delay: const Duration(seconds: 1),
          future: () async {
            final uri = Uri.https(
              'api.punkapi.com',
              '/v2/beers',
              {
                'page': page.toString(),
                'per_page': '10',
              },
            );

            final request = await httpClient.getUrl(uri);
            final response = await request.close();

            if (response.statusCode == HttpStatus.ok) {
              final List<dynamic> data = json.decode(
                await response.transform(utf8.decoder).join(),
              );

              return data.map((beer) => BeerModel.fromJson(beer));
            } else {
              throw const DataErrorUnavailable();
            }
          });
    } catch (error) {
      if (error is DataError) {
        rethrow;
      } else {
        throw const DataErrorUnknown();
      }
    }
  }
}
