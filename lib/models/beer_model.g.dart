// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BeerModel _$BeerModelFromJson(Map<String, dynamic> json) => BeerModel(
      name: json['name'] as String,
      imageUrl: json['image_url'] as String,
      tagline: json['tagline'] as String,
      description: json['description'] as String,
    );

Map<String, dynamic> _$BeerModelToJson(BeerModel instance) => <String, dynamic>{
      'name': instance.name,
      'image_url': instance.imageUrl,
      'tagline': instance.tagline,
      'description': instance.description,
    };
