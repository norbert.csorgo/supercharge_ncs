import 'dart:convert' show json;
import 'package:json_annotation/json_annotation.dart';
import 'package:objectbox/objectbox.dart';

part 'beer_model.g.dart';

@Entity()
@JsonSerializable()
class BeerModel {
  BeerModel({
    //ObjectBox req.
    this.id = 0,
    required this.name,
    required this.imageUrl,
    required this.tagline,
    required this.description,
  });
  @JsonKey(includeFromJson: false)
  int id;
  @Unique(onConflict: ConflictStrategy.replace)
  final String name;

  @JsonKey(name: "image_url")
  final String imageUrl;
  final String tagline;
  final String description;

  @override
  String toString() => toJson().toString();

  factory BeerModel.fromJson(Map<String, dynamic> json) =>
      _$BeerModelFromJson(json);
  factory BeerModel.fromJsonString(String jsonString) =>
      BeerModel.fromJson(json.decode(jsonString));
  Map<String, dynamic> toJson() => _$BeerModelToJson(this);
}
