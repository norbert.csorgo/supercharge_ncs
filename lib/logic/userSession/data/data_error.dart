import 'package:flutter/foundation.dart' show immutable;
import 'package:supercharge_ncs/utilities/exception_base.dart';

@immutable
sealed class DataError extends DataExceptionBase {
  const DataError({
    required super.dialogTitle,
    required super.dialogText,
  });
}

@immutable
final class DataErrorUnknown extends DataError {
  const DataErrorUnknown()
      : super(
          dialogTitle: 'Data error',
          dialogText: 'Unknown error',
        );
}

@immutable
final class DataErrorUnavailable extends DataError {
  const DataErrorUnavailable()
      : super(
          dialogTitle: 'Data error',
          dialogText: 'Service not available.',
        );
}
