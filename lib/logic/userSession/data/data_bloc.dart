import 'dart:async';
import 'package:flutter/foundation.dart' show immutable;
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:supercharge_ncs/datasource/local/local_datasource.dart';
import 'package:supercharge_ncs/datasource/network/network_datasource.dart';
import 'package:supercharge_ncs/logic/userSession/data/data_error.dart';
import 'package:supercharge_ncs/models/beer_model.dart';
import 'package:supercharge_ncs/res/types.dart';
import 'package:supercharge_ncs/utilities/future_extensions.dart';
import 'package:supercharge_ncs/utilities/stream_extensions.dart';

@immutable
sealed class DataBlocInterface {
  Stream<LoadingInfo> get isLoading;
  Stream<DataError?> get dataErrors;
  Stream<List<BeerModel>> get savedBeers;
  Stream<Iterable<BeerModel>> get beers;

  void dispose();
}

@immutable
class DataBloc implements DataBlocInterface {
  final BehaviorSubject<LoadingInfo> _isLoading;
  final BehaviorSubject<DataError?> _dataErrorsSubject;

  @override
  Stream<LoadingInfo> get isLoading => _isLoading.stream;
  @override
  Stream<DataError?> get dataErrors => _dataErrorsSubject.stream;

  @override
  final Stream<Iterable<BeerModel>> beers;
  final Sink<void> beerPageSink;

  final BehaviorSubject<BeerModel> _saveBeerSubject;
  Sink<BeerModel> get saveBeerSink => _saveBeerSubject.sink;

  @override
  Stream<List<BeerModel>> get savedBeers => _savedBeersSubject.stream;
  final BehaviorSubject<List<BeerModel>> _savedBeersSubject;
  final Sink<void> fetchSavedBeersSink;

  const DataBloc._({
    required BehaviorSubject<LoadingInfo> isLoading,
    required BehaviorSubject<DataError?> dataErrorsSubject,
    required this.beers,
    required this.beerPageSink,
    required this.fetchSavedBeersSink,
    required BehaviorSubject<List<BeerModel>> savedBeersSubject,
    required BehaviorSubject<BeerModel> saveBeerSubject,
  })  : _isLoading = isLoading,
        _dataErrorsSubject = dataErrorsSubject,
        _saveBeerSubject = saveBeerSubject,
        _savedBeersSubject = savedBeersSubject;

  factory DataBloc() {
    final isLoadingSubject = BehaviorSubject<LoadingInfo>();
    final dataErrorSubject = BehaviorSubject<DataError>();

    final beerPageSink = BehaviorSubject<void>();

    final fetchSavedBeersSink = BehaviorSubject<void>();
    final saveBeerSubject = BehaviorSubject<BeerModel>();
    final savedBeers = BehaviorSubject<List<BeerModel>>.seeded([]);

    int page = 1;

    final beerStream =
        beerPageSink.startLoading(onSink: isLoadingSubject).asyncMap((_) async {
      savedBeers.add([]);
      final savedElements = await LocalDatasource.localStorage
          .getSavedBeerModels()
          .onErrorJustReturn([]);
      for (final element in savedElements) {
        await LocalDatasource.localStorage.removeSavedBeerModel(id: element.id);
      }
      final result =
          await NetworkDatasource.data.getBeers(page).onError((error, _) {
        dataErrorSubject.add(
          error is DataError ? error : const DataErrorUnknown(),
        );
        return [];
      });
      page++;
      return result;
    }).stopLoading(onSink: isLoadingSubject);

    fetchSavedBeersSink
        .startLoading(onSink: isLoadingSubject)
        .asyncMap((_) async {
          savedBeers.add(await LocalDatasource.localStorage
              .getSavedBeerModels()
              .onError((error, _) {
            dataErrorSubject.add(const DataErrorUnavailable());
            return [];
          }));
        })
        .stopLoading(onSink: isLoadingSubject)
        .drain();

    saveBeerSubject.asyncMap((beer) async {
      final error =
          await LocalDatasource.localStorage.saveBeerModel(beer: beer);

      if (error == null) {
        savedBeers.add([...savedBeers.value, beer]);
      }
    }).drain();

    return DataBloc._(
      isLoading: isLoadingSubject,
      dataErrorsSubject: dataErrorSubject,
      beers: beerStream.asBroadcastStream(),
      beerPageSink: beerPageSink,
      fetchSavedBeersSink: fetchSavedBeersSink,
      savedBeersSubject: savedBeers,
      saveBeerSubject: saveBeerSubject,
    );
  }
  @override
  void dispose() {
    _isLoading.close();
    _dataErrorsSubject.close();
    _saveBeerSubject.close();
    fetchSavedBeersSink.close();
    beerPageSink.close();
  }
}
