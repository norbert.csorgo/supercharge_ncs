import 'package:flutter/foundation.dart' show immutable;
import 'package:rxdart/rxdart.dart';
import 'package:supercharge_ncs/res/enums.dart';

@immutable
class ViewBloc {
  final Sink<Views> goToView;
  final Stream<Views> currentView;

  const ViewBloc._({
    required this.goToView,
    required this.currentView,
  });

  factory ViewBloc() {
    final goToViewSubject = BehaviorSubject<Views>();

    return ViewBloc._(
      goToView: goToViewSubject,
      currentView: goToViewSubject.startWith(
        Views.initial,
      ),
    );
  }

  void dispose() {
    goToView.close();
  }
}
