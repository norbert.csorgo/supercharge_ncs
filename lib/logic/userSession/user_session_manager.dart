import 'dart:async';
import 'package:flutter/material.dart';
import 'package:supercharge_ncs/logic/userSession/data/data_bloc.dart';
import 'package:supercharge_ncs/logic/userSession/views/view_bloc.dart';
import 'package:supercharge_ncs/models/beer_model.dart';
import 'package:supercharge_ncs/res/enums.dart';

export 'views/view_bloc.dart' show ViewBloc;
export 'data/data_bloc.dart' show DataBloc;

@immutable
sealed class UserSessionInterface {
  Stream<Views> get baseView;
  Sink<Views> get goToView;
  Stream<Iterable<BeerModel>> get beers;
  Stream<List<BeerModel>> get savedBeers;
  Sink<void> get fetchSavedBeersSink;
  Sink<BeerModel> get saveBeerSink;

  void dispose();
}

@immutable
final class UserSessionManager implements UserSessionInterface {
  const UserSessionManager._({
    required ViewBloc viewBloc,
    required DataBloc dataBloc,
    required StreamSubscription<Views> viewListener,
  })  : _viewBloc = viewBloc,
        _dataBloc = dataBloc,
        _viewListener = viewListener;

  factory UserSessionManager({
    required ViewBloc viewBloc,
    required DataBloc dataBloc,
  }) {
    final viewListener = viewBloc.currentView.distinct().listen((event) {
      if (event == Views.decision) {
        dataBloc.beerPageSink.add(null);
      }
    });
    return UserSessionManager._(
      viewBloc: viewBloc,
      dataBloc: dataBloc,
      viewListener: viewListener,
    );
  }
  final StreamSubscription<Views> _viewListener;
  final ViewBloc _viewBloc;
  final DataBloc _dataBloc;

  @override
  void dispose() {
    _viewBloc.dispose();
    _dataBloc.dispose();
    _viewListener.cancel();
  }

  @override
  Stream<Views> get baseView => _viewBloc.currentView;

  @override
  Sink<Views> get goToView => _viewBloc.goToView;

  @override
  Stream<Iterable<BeerModel>> get beers => _dataBloc.beers;

  @override
  Sink<void> get fetchSavedBeersSink => _dataBloc.fetchSavedBeersSink;

  @override
  Sink<BeerModel> get saveBeerSink => _dataBloc.saveBeerSink;

  @override
  Stream<List<BeerModel>> get savedBeers => _dataBloc.savedBeers;
}
