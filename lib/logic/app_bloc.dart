import 'dart:async';
import 'package:flutter/foundation.dart' show immutable;
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:supercharge_ncs/logic/userSession/user_session_manager.dart';
import 'package:supercharge_ncs/res/types.dart';
import 'package:supercharge_ncs/utilities/exception_base.dart'
    show ExceptionBase;

@immutable
final class AppBloc {
  final UserSessionManager userSession;
  final Stream<LoadingInfo> isLoading;
  final Stream<ExceptionBase?> appErrors;

  factory AppBloc() {
    final viewBloc = ViewBloc();
    final dataBloc = DataBloc();

    final isLoading = Rx.merge([
      dataBloc.isLoading,
    ]);

    final appErrors = Rx.merge([
      dataBloc.dataErrors,
    ]);

    final userSessionManager = UserSessionManager(
      viewBloc: viewBloc,
      dataBloc: dataBloc,
    );

    return AppBloc._(
      userSession: userSessionManager,
      isLoading: isLoading.asBroadcastStream(),
      appErrors: appErrors,
    );
  }

  void dispose() {
    userSession.dispose();
  }

  const AppBloc._({
    required this.userSession,
    required this.isLoading,
    required this.appErrors,
  });
}
