import 'package:flutter_screenutil/flutter_screenutil.dart';

abstract final class AppConsts {
  static const designWidth = 400.0;
  static const designHeight = 800.0;

  static const time5000 = 5000;
  static const time2000 = 2000;
  static const time1250 = 1250;
  static const time1000 = 1000;
  static const time750 = 750;
  static const time500 = 500;
  static const time250 = 250;
  static const time15 = 15;
  static const time10 = 10;

  static const zero = 0.0;
  static const half = 0.5;
  static const normal1 = 1.0;
  static const normal2 = 2.0;
  static const normal4 = 4.0;
  static const normal6 = 6.0;
  static const normal8 = 8.0;
  static const normal12 = 12.0;
  static const normal16 = 16.0;
  static const normal24 = 24.0;
  static const normal32 = 32.0;
  static const normal36 = 36.0;
  static const normal40 = 40.0;
  static const normal48 = 48.0;
  static const normal64 = 64.0;
  static const normal80 = 80.0;
  static const normal120 = 120.0;
  static const normal240 = 240.0;

  static final high1 = normal1.h;
  static final high2 = normal2.h;
  static final high4 = normal4.h;
  static final high6 = normal6.h;
  static final high8 = normal8.h;
  static final high12 = normal12.h;
  static final high16 = normal16.h;
  static final high24 = normal24.h;
  static final high32 = normal32.h;
  static final high36 = normal36.h;
  static final high40 = normal40.h;
  static final high48 = normal48.h;
  static final high64 = normal64.h;
  static final high80 = normal80.h;
  static final high120 = normal120.h;
  static final high240 = normal240.h;

  static final wide1 = normal1.w;
  static final wide2 = normal2.w;
  static final wide4 = normal4.w;
  static final wide6 = normal6.w;
  static final wide8 = normal8.w;
  static final wide12 = normal12.w;
  static final wide16 = normal16.w;
  static final wide24 = normal24.w;
  static final wide32 = normal32.w;
  static final wide36 = normal36.w;
  static final wide40 = normal40.w;
  static final wide48 = normal48.w;
  static final wide64 = normal64.w;
  static final wide80 = normal80.w;
  static final wide120 = normal120.w;

  static final rad1 = normal1.r;
  static final rad2 = normal2.r;
  static final rad4 = normal4.r;
  static final rad6 = normal6.r;
  static final rad8 = normal8.r;
  static final rad12 = normal12.r;
  static final rad16 = normal16.r;
  static final rad24 = normal24.r;
  static final rad32 = normal32.r;
  static final rad36 = normal36.r;
  static final rad40 = normal40.r;
  static final rad48 = normal48.r;
  static final rad64 = normal64.r;
  static final rad80 = normal80.r;
  static final rad120 = normal120.r;
}

abstract final class NetworkStrings {
  NetworkStrings._();
}

abstract final class RiveStrings {
  RiveStrings._();
  static const loadingAB = "New Artboard";
  static const loadingTimeline = "Timeline 1";
}
