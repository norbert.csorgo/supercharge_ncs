import 'package:supercharge_ncs/res/enums.dart';

typedef LoadingInfo = ({
  bool isLoading,
  LoadingEvent? comment,
});
