import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:supercharge_ncs/res/app_consts.dart';
import 'package:supercharge_ncs/res/gen/fonts.gen.dart';

@immutable
abstract final class AppTheme {
  static ThemeData main = ThemeData.light().copyWith(
    textTheme: _textTheme,
    colorScheme: ColorScheme.fromSeed(seedColor: gold100),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    scaffoldBackgroundColor: gold50,
    appBarTheme: const AppBarTheme(
      backgroundColor: gold200,
      surfaceTintColor: gold200,
      scrolledUnderElevation: AppConsts.zero,
      shadowColor: transparent,
      systemOverlayStyle: systemUiOverlayStyle,
    ),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        shape: MaterialStatePropertyAll(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(AppConsts.rad6),
          ),
        ),
        foregroundColor: MaterialStateProperty.resolveWith<Color>(
          (states) {
            if (states.contains(MaterialState.pressed)) {
              return gold400;
            } else if (states.contains(MaterialState.disabled)) {
              return white.withOpacity(AppConsts.half);
            } else {
              return gold800;
            }
          },
        ),
      ),
    ),
    cardTheme: CardTheme(
      color: gold100,
      elevation: AppConsts.normal4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(AppConsts.rad8),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
      elevation: const MaterialStatePropertyAll(AppConsts.normal4),
      surfaceTintColor: const MaterialStatePropertyAll(transparent),
      shape: MaterialStatePropertyAll(RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(AppConsts.rad6)))),
      backgroundColor: MaterialStateProperty.resolveWith<Color>((states) {
        if (states.contains(MaterialState.pressed)) {
          return gold400;
        } else if (states.contains(MaterialState.disabled)) {
          return white.withOpacity(AppConsts.half);
        } else {
          return gold800;
        }
      }),
      foregroundColor: MaterialStateProperty.resolveWith<Color>((states) {
        if (states.contains(MaterialState.pressed)) {
          return gold700;
        } else if (states.contains(MaterialState.disabled)) {
          return gold800.withOpacity(0.25);
        } else {
          return gold100;
        }
      }),
    )),
    radioTheme: const RadioThemeData(
      fillColor: MaterialStatePropertyAll(gold1000),
    ),
    switchTheme: SwitchThemeData(
      thumbColor: MaterialStateProperty.resolveWith<Color?>(
        (states) {
          if (states.contains(MaterialState.selected) ||
              states.contains(MaterialState.focused)) {
            return gold900;
          }
          if (states.contains(MaterialState.disabled)) {
            return black.withOpacity(0.25);
          }

          return null;
        },
      ),
      trackColor: MaterialStateProperty.resolveWith<Color?>(
        (states) {
          if (states.contains(MaterialState.selected) ||
              states.contains(MaterialState.focused)) {
            return gold400;
          }
          if (states.contains(MaterialState.disabled)) {
            return black.withOpacity(0.25);
          }

          return null;
        },
      ),
      overlayColor: MaterialStateProperty.resolveWith<Color?>(
        (states) {
          if (states.contains(MaterialState.selected) ||
              states.contains(MaterialState.focused)) {
            return gold400.withOpacity(AppConsts.half);
          }
          if (states.contains(MaterialState.disabled)) {
            return black.withOpacity(0.25);
          }

          return null;
        },
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      hintStyle: _textTheme.titleMedium?.copyWith(color: gold1100),
      fillColor: gold100,
      filled: true,
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: gold700, width: 0.5),
        borderRadius: BorderRadius.circular(AppConsts.rad6),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: gold700, width: 0.5),
        borderRadius: BorderRadius.circular(AppConsts.rad6),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: gold700, width: 0.5),
        borderRadius: BorderRadius.circular(AppConsts.rad6),
      ),
      labelStyle: _textTheme.bodyLarge?.copyWith(color: gold800),
    ),
    unselectedWidgetColor: Colors.black87,
    dividerTheme: const DividerThemeData(color: transparent),
  );

  static final TextTheme _textTheme = Typography.blackMountainView.apply(
    fontFamily: FontFamily.jost,
    bodyColor: gold1100,
    displayColor: gold1100,
  );

  static const systemUiOverlayStyle = SystemUiOverlayStyle(
    systemNavigationBarColor: gold50,
    systemNavigationBarDividerColor: gold50,
    systemNavigationBarContrastEnforced: false,
    statusBarColor: transparent,
    statusBarBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.dark,
    systemStatusBarContrastEnforced: false,
    systemNavigationBarIconBrightness: Brightness.dark,
  );

  static const white = Color.fromARGB(255, 255, 255, 255);
  static const black = Color.fromARGB(255, 0, 0, 0);
  static const transparent = Color.fromARGB(0, 0, 0, 0);

  static const Color gold50 = Color(0xFFFFFEFC);
  static const Color gold100 = Color(0xFFFDFBF3);
  static const Color gold200 = Color(0xFFF7F1DE);
  static const Color gold300 = Color(0xFFEEDCBE);
  static const Color gold400 = Color(0xFFE6D89F);
  static const Color gold500 = Color(0xFFD3BF6E);
  static const Color gold600 = Color(0xFFC8A744);
  static const Color gold700 = Color(0xFFB38F22);
  static const Color gold800 = Color(0xFF9F6B16);
  static const Color gold900 = Color(0xFF855912);
  static const Color gold1000 = Color(0xFF6A470F);
  static const Color gold1100 = Color(0xFF50360B);
  static const Color goldShadow = Color(0xFF100B02);

  static const gradientColors = [gold300, white];
}
