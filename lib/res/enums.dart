enum LoadingEvent {
  fetching,
  saving,
  deleting,
}

enum Views {
  initial,
  decision,
  list,
}
