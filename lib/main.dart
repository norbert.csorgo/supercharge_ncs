import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:rive/rive.dart';
import 'package:supercharge_ncs/logic/app_bloc.dart';
import 'package:supercharge_ncs/res/app_consts.dart';
import 'package:supercharge_ncs/res/app_theme.dart';
import 'package:supercharge_ncs/utilities/animation_preloader.dart';
import 'package:supercharge_ncs/view/components/loading_overlay.dart';
import 'package:supercharge_ncs/view/root_widget.dart';

void main() async {
  await initApp();
  runApp(const App());
}

Future<void> initApp() async {
  WidgetsFlutterBinding.ensureInitialized();

  await LoadingOverlay.initialize(
    defaultBackgroundColor: AppTheme.gold200,
    defaultLoadingWidget: AspectRatio(
      aspectRatio: 1,
      child: Consumer<AnimationPreloader>(
        child: const CircularProgressIndicator(color: AppTheme.goldShadow),
        builder: (context, preloader, loadingWidget) =>
            preloader.animationFile != null
                ? RiveAnimation.direct(
                    preloader.animationFile!,
                    artboard: RiveStrings.loadingAB,
                    placeHolder: loadingWidget,
                    fit: BoxFit.contain,
                    animations: const [RiveStrings.loadingTimeline],
                  )
                : loadingWidget!,
      ),
    ),
  );
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => AnimationPreloader(),
      child: Provider(
        create: (_) => AppBloc(),
        dispose: (_, bloc) => bloc.dispose(),
        child: ScreenUtilInit(
          designSize: const Size(AppConsts.designWidth, AppConsts.designHeight),
          useInheritedMediaQuery: true,
          builder: (context, _) => MaterialApp(
            debugShowCheckedModeBanner: false,
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            theme: AppTheme.main,
            home: const RootWidget(),
          ),
        ),
      ),
    );
  }
}
