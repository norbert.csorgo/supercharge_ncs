## Comments for SC reviewer

- Localization was started but was left out due to time constraints.
- One beer-session is stored at a time. Upon initiating a new selection, the previously saved items will get deleted but if the user exits instead, they can still view their last session's results next time. It could be finalized with a full specification and end-goals.
- Tests were left out due to time constraints.
- Displaying errors was preconfigured in RootWidget but was left out due to time constraints.
- Test recording is available at ./simulator_recording.mp4
